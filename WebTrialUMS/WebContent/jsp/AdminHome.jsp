<%@page import="com.data.*"%>
<%@page import="com.connector.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome Admin</title>
<style>
body {
	font: 18px tahoma;
	margin: 0;
}

ul.horizontal {
	list-style-type: none;
	margin: 0;
	padding: 0;
	top: 0;
	width: 100%;
	background-color: #FF9900;
	position: fixed;
	overflow: hidden;
	border: 3px solid #CC292A;
}

li.horizontal {
	float: right;
}

li.horizontal a {
	display: block;
	color: black;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.horizontal a:hover:not (.active ) {
	background-color: #555;
	color: white;
}

.active {
	background-color: #CC292A;
	color: white;
}

ul.vertical {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 25%;
	background-color: #FF9900;
	position: fixed;
	height: 100%;
	overflow: auto;
	border: 3px solid #CC292A;
}

li.vertical a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.vertical a.active {
	background-color: #CC292A;
	color: white;
}

li.vertical a:hover:not (.active ) {
	background-color: #555;
	color: white;
}
</style>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Expires", "0");
	response.setDateHeader("Expires", -1);
%>

</head>
<body>
	<%
		try {
			if (session.getAttribute("userid") == null) {
				request.setAttribute("Error", "Your Session has ended. Please login again...");
				request.getRequestDispatcher("/jsp/Login.jsp").forward(request, response);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	%>


	<ul class="horizontal">
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/LogOutServlet">Log Out</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/ChangePassword.jsp">Change
				Password</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/EditProfileAdmin.jsp">Edit
				Profile</a></li>
		<li class="horizontal"><a class="active"
			href="${pageContext.request.contextPath }/jsp/AdminHome.jsp">My
				Profile</a></li>
		<li class="horizontal"
			style="float: left; display: block; text-align: center; padding: 14px 16px; font-size: 30px;">Welcome
			Admin</li>
	</ul>

	<ul class="vertical">

		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UserList.jsp">View
				User List</a></li>
		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UpdateRole.jsp">Update
				Role</a></li>
	</ul>

	<div
		style="margin-left: 25%; margin-top: 70px; padding: 1px 16px; height: 1000px;">

		<%
			String id = request.getSession().getAttribute("userid").toString();
			int uId = Integer.parseInt(id);
			User user = UserDAO.getUserDetails(uId);
		%>
		<br>
		<br>
		<fieldset>

			<table width="100%">
				<tr>
					<td>Name:</td>
					<td><%=user.getName()%></td>
				</tr>
				<tr>
					<td>DOB:</td>
					<td><%=user.getDob()%></td>
				</tr>
				<tr>
					<td>Age:</td>
					<td><%=user.getAge()%></td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td><%=user.getGender()%></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><%=user.getAddress()%></td>
				</tr>

				<tr>
					<td>City:</td>
					<td><%=user.getCity()%></td>
				</tr>
				<tr>
					<td>State:</td>
					<td><%=user.getState()%></td>
				</tr>
				<tr>
					<td>Country:</td>
					<td><%=user.getCountry()%></td>
				</tr>
				<tr>
					<td>PinCode:</td>
					<td><%=user.getPinCode()%></td>
				</tr>
				<tr>
					<td>Phone Number:</td>
					<td><%=user.getPhoneNumber()%></td>
				</tr>
				<tr>
					<td>Email ID:</td>
					<td><%=user.getEmailId()%></td>
				</tr>
				
			</table>
			<br>
		</fieldset>
	</div>
</body>
</html>