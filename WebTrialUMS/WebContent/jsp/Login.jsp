<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Management System</title>
<style>
#submission {
	text-align: center;
}

#login {
	margin: 50px auto;
	width: 400px;
	font-family: tahoma;
}

body {
	font: 18px tahoma;
	margin: 0;
	background: #FF9900;
}

fieldset {
	border: 3px solid #CC292A;
	background: White;
	font-family: tahoma;
}

input[type="password"], input[type="text"] {
	border: 1px solid #CC292A;
	border-radius: 4px;
	box-shadow: 0 1px #fff;
	box-sizing: border-box;
	color: black;
	height: 39px;
	margin: 31px 0 0 29px;
	padding-left: 37px;
	transition: box-shadow 0.3s;
	width: 240px;
	font-family: tahoma;
}

input[type="submit"] {
	font-family: tahoma;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 25%;
}
</style>
</head>
<body>
	<p align="center" style="font-family: tahoma; font-size: 250%;">Welcome
		to User Management System</p>
	<form action="${pageContext.request.contextPath}/LoginServlet"	method="post">
		<div id="login">
			<fieldset style="bgcolor: #CBFFA;">
				<strong style="font-family: 23px tahoma;"> LOG IN </strong>
				<table style="width: 100%">
					<tbody>
						<tr>
							<td><input type="text" name="Username" id="username"
								placeholder="Username" required></td>
						</tr>
						<tr>
							<td><input type="password" name="Password" id="password"
								placeholder="Password" required></td>
						</tr>
					</tbody>
				</table>
				<br> <a
					href="${pageContext.request.contextPath  }/jsp/PasswordRecovery.jsp"
					style="font-family: tahoma; margin: 31px 0 0 29px; color: Black; font: 16px tahoma">Forgot
					Password?</a><br>
				<br> <input type="submit" value="Log In"
					style="font-size: 100%; width: 150px; height: 50px; margin-left: 10%; font-family: tahoma">
			</fieldset>
		</div>
	</form>
	<p align="center">
		<font color="red"><%=(request.getAttribute("invalidUser") == null) ? "" : request.getAttribute("invalidUser")%></font>
		<%=request.getAttribute("activestatus") == null ? "" : request.getAttribute("activestatus")%>
		<%=request.getAttribute("deactstatus") == null ? "" : request.getAttribute("deactstatus")%>
		<%=request.getAttribute("Error") == null ? "" : request.getAttribute("Error")%>
	<form action="${pageContext.request.contextPath }/jsp/SignUpPage.jsp"
		method="post">
		<p id="submission">
			<input type="submit" value="New user? Click here to Sign Up"
				style="font-size: 100%; height: 50px; width: 400px; background: White; border: 3px solid #CC292A; font-family: tahoma;" />
		</p>
	</form>
</body>
</html>
