<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Warning</title>
<script>var path = "${pageContext.request.contextPath}"</script>
<script type="text/javascript">
	function Activatebutton()
	{
        location.href = path + "/jsp/Active.jsp";
    }
</script>
<style>
#submission
{
text-align:center;
}
#login
{
margin: 50px auto;
width: 500px;

}
body
{
font:18px tahoma;
margin:0;
background:#FF9900;
}
fieldset
{
border:3px solid #CC292A;
background:White;

}

input[type="submit"]

{
  font-family:tahoma;
  font-size:18px;
  border: 1px solid Black;
  border-radius: 55px;
  box-shadow: 0 1px #fff;
  background-color:#CC292A;
  width:100%;
  height:30px;
  color:black;
}
</style>
</head>
<body>
<div id="login">
<fieldset>
<h1 align="center">Status of User</h1>
<p align="center"><font color="red"><%=request.getAttribute("recoveryStatus")==null?"":request.getAttribute("recoveryStatus") %></font></p>


<br>
<table style = "width:90%" >
<tr>
<td>

Do you wish to activate your account? Press the activate button
</td>
<td>

<input type="submit"  value="Activate" style = "font:18px tahoma;" id="Act" onclick="Activatebutton()">
</td>
</tr>
</table>
<br>
</div>
</fieldset>

<p align="center"><a href="${pageContext.request.contextPath }/jsp/Login.jsp" style="color:black">Back to login Page</a></p>
</body>
</html>
