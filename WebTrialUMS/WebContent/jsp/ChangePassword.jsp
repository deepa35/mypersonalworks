<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password</title>
<script>
	function formvalidate() {
		var password = document.getElementById("password").value;
		var passlen = password.length;
		if (passlen < 6) {
			alert("Password is too short. Atleast 6 characters needed.");
			return false;
		}

		else if (password.search(/\d/) == -1) {
			alert("Atleast one digit needed in password");
			return false;
		} else if (password.search(/[a-zA-Z]/) == -1) {
			alert("Atleast one alphabet needed in password");
			return false;
		} else if (password.search(/[!\@\#\$\%\^\&\*\(\)\_\+]/) == -1) {
			alert("Atleast one special character needed in password");
			return false;
		}
		var confirmpassword = document.getElementById("confirmpassword").value;
		if (password != confirmpassword) {
			alert("Please make sure that your password and corfirmed password are the same");
			return false;
		}
	}
</script>
<style>
body {
	font: 18px tahoma;
	margin: 0;
}

ul.horizontal {
	list-style-type: none;
	margin: 0;
	padding: 0;
	top: 0;
	width: 100%;
	background-color: #FF9900;
	position: fixed;
	overflow: hidden;
	border: 3px solid #CC292A;
}

li.horizontal {
	float: right;
}

li.horizontal a {
	display: block;
	color: black;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.horizontal a:hover:not (.active ) {
	background-color: #555;
	color: white;
}

.active {
	background-color: #CC292A;
	color: white;
}

ul.vertical {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 25%;
	background-color: #FF9900;
	position: fixed;
	height: 100%;
	overflow: auto;
	border: 3px solid #CC292A;
}

li.vertical a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.vertical a.active {
	background-color: #CC292A;
	color: white;
}

li.vertical a:hover:not (.active ) {
	background-color: #555;
	color: white;
}

input[type="submit"] {
	font-family: 18px tahoma;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 10%;
	color: black;
}
</style>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Expires", "0");
	response.setDateHeader("Expires", -1);
%>

</head>
<body>
	<%
		try {
			if (session.getAttribute("userid") == null) {
				request.setAttribute("Error", "Your Session has ended. Please login again...");
				request.getRequestDispatcher("/jsp/Login.jsp").forward(request, response);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	%>
	<ul class="horizontal">
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/LogOutServlet">Log Out</a></li>
		<li class="horizontal"><a class="active"
			href="${pageContext.request.contextPath }/jsp/ChangePassword.jsp">Change
				Password</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/EditProfileAdmin.jsp">Edit
				Profile</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/AdminHome.jsp">My
				Profile</a></li>
		<li class="horizontal"
			style="float: left; display: block; text-align: center; padding: 14px 16px; font-size: 30px;">Welcome
			Admin</li>
	</ul>

	<ul class="vertical">

		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UserList.jsp">View
				User List</a></li>
		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UpdateRole.jsp">Update
				Role</a></li>

	</ul>

	<div
		style="margin-left: 25%; margin-top: 70px; padding: 1px 16px; height: 1000px;">

		<form
			action="${pageContext.request.contextPath }/ChangePasswordServlet"
			method="post" onsubmit="return formvalidate()">
			<br>
			<table>
				<tbody>
					<tr>
						<td>Enter new password:</td>
						<td><input type="password" name="newpassword" id="password"
							required></td>
					</tr>
					<tr>
						<td>Re-enter new password:</td>
						<td><input type="password" name="newpassword1"
							id="confirmpassword" required></td>
					</tr>
				</tbody>
			</table>
			<br> <input type="submit" value="Submit"
				style="font: 18px tahoma;">
		</form>
		<%=request.getAttribute("passwordchangestatus") == null ? ""
					: request.getAttribute("passwordchangestatus")%>
	</div>
</body>
</html>