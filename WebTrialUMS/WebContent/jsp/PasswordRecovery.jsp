<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Password Recovery</title>
<style>
#submission {
	text-align: center;
}

#login {
	margin: 50px auto;
	width: 500px;
}

body {
	font: 18px tahoma;
	margin: 0;
	background: #FF9900;
}

fieldset {
	border: 3px solid #CC292A;
	background: White;
}

input[type="submit"] {
	font-family: tahoma;
	font-size: 18px;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 15%;
	height: 30px;
	color: black;
}
</style>
</head>
<body>
	<h1 align="center">Password recovery</h1>
	<form method="post"
		action="${pageContext.request.contextPath }/PasswordRecServlet">
		<div id="login">
			<fieldset>
				<table style="width: 100%">
					<tbody>
						<tr>
							<td>Please enter your username:</td>
							<td><input type="text" name="usernamerec" required></td>
						</tr>
						<tr>
							<td>Enter your date of birth:</td>
							<td><input type="date" name="dob" id="dob" required></td>
						</tr>
					</tbody>
				</table>
				<br> <input type="submit" value="Submit"
					style="float: left; font: 18px tahoma; width: 150px;">
			</fieldset>
		</div>
		<p align="center"><%=request.getAttribute("recoveryStatus") == null ? ""
					: "Your password is '" + request.getAttribute("recoveryStatus") + "'"%></p>
		<p align="center">
			<a href="${pageContext.request.contextPath }/jsp/Login.jsp"
				style="color: black">Back to login</a>
		</p>
	</form>
</body>
</html>