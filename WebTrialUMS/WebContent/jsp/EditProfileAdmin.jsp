<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.data.*"%>
<%@ page import="com.connector.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Profile Page</title>
<script>
	function ValidateDetails() {
		alert("Are you sure you want to submit??");
		var gender = document.getElementsByName("gender");
		if (gender[0].checked == false && gender[1].checked == false) {
			alert("Please select gender");
			return false;
		}
		var pincode = document.getElementById("pincode").value;
		if (isNaN(pincode) || pincode.length != 6) {
			alert("Please enter a valid numeric pincode of 6 digits");
			return false;
		}

		var tel = document.signup.phonenumber.value;
		var phoneValid = tel.replace(/[^0-9]/g, '');
		if (isNaN(tel) == true || phoneValid.length != 10) {
			alert("Please enter valid phone number");
			return false;
		}

		var email = document.getElementById("emailid");
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email.value)) {
			alert("Please provide a valid email address");
			email.focus;
			return false;
		}
	}
</script>
<style>
body {
	font: 18px tahoma;
	margin: 0;
}

ul.horizontal {
	list-style-type: none;
	margin: 0;
	padding: 0;
	top: 0;
	width: 100%;
	background-color: #FF9900;
	position: fixed;
	overflow: hidden;
	border: 3px solid #CC292A;
}

li.horizontal {
	float: right;
}

li.horizontal a {
	display: block;
	color: black;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.horizontal a:hover:not (.active ) {
	background-color: #555;
	color: white;
}

.active {
	background-color: #CC292A;
	color: white;
}

ul.vertical {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 25%;
	background-color: #FF9900;
	position: fixed;
	height: 100%;
	overflow: auto;
	border: 3px solid #CC292A;
}

li.vertical a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
	border: 1px solid #CC292A;
}

li.vertical a.active {
	background-color: #CC292A;
	color: white;
}

li.vertical a:hover:not (.active ) {
	background-color: #555;
	color: white;
}

input[type="submit"] {
	font-family: tahoma;
	font-size: 18px;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 15%;
	height: 30px;
	color: black;
}
</style>
<%
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Expires", "0");
	response.setDateHeader("Expires", -1);
%>
</head>
<body>
	<%
		try {
			if (session.getAttribute("userid") == null) {
				request.setAttribute("Error", "Your Session has ended. Please login again...");
				request.getRequestDispatcher("/jsp/Login.jsp").forward(request, response);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	%>
	<ul class="horizontal">
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/LogOutServlet">Log Out</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/ChangePassword.jsp">Change
				Password</a></li>
		<li class="horizontal"><a class="active"
			href="${pageContext.request.contextPath }/jsp/EditProfileAdmin.jsp">Edit
				Profile</a></li>
		<li class="horizontal"><a
			href="${pageContext.request.contextPath }/jsp/AdminHome.jsp">My
				Profile</a></li>
		<li class="horizontal"
			style="float: left; display: block; text-align: center; padding: 14px 16px; font-size: 30px;">Welcome
			Admin</li>
	</ul>

	<ul class="vertical">

		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UserList.jsp">View
				User List</a></li>
		<li class="vertical"><a
			href="${pageContext.request.contextPath }/jsp/UpdateRole.jsp">Update
				Role</a></li>

	</ul>
	<div
		style="margin-left: 25%; margin-top: 70px; padding: 1px 16px; height: 1000px;">

		<div id="login">
			<p align="right">
				<font color="red">*</font>Mandatory Fields
			</p>
			<form action="${pageContext.request.contextPath }/EditProfileServlet"
				method="post" name="signup" onsubmit="return ValidateDetails()">
				<fieldset>
					<p style="text-decoration: underline">Personal Details</p>
					<%
						String id = request.getSession().getAttribute("userid").toString();
						int uId = Integer.parseInt(id);
						User user = UserDAO.getUserDetails(uId);
					%>
					<table>
						<tr>

							<td>Name<font color="red">*</font></td>
							<td><input type="text" name="name"
								value="<%=user.getName()%>" required></td>
						</tr>

						<tr>
							<td>Gender<font color="red">*</font></td>
							<%
								if (user.getGender().equals("female")) {
							%>
							<td><input type="radio" name="gender" value="female" checked>Female</td>
							<td><input type="radio" name="gender" value="male">Male</td>
							<%
								} else if (user.getGender().equals("male")) {
							%>
							<td><input type="radio" name="gender" value="female">Female</td>
							<td><input type="radio" name="gender" value="male" checked>Male</td>
							<%
								}
							%>
						
						<tr>
							<td>Date Of Birth<font color="red">*</font></td>
							<td><input type="date" name="dateofbirth"
								value="<%=user.getDob()%>" required></td>
							<td>Age</td>
							<td><input type="text" name="age"
								value="<%=user.getAge()%>"></td>
						</tr>
						<tr>

							<%
								String addressline = user.getAddress();
							%>
							<td>Addressline 1<font color="red">*</font></td>
							<td><input type="text" name="address1"
								value="<%=addressline%>" required></td>
						</tr>
						<tr>
							<td>City<font color="red">*</font></td>
							<td><input type="text" name="city"
								value="<%=user.getCity()%>" required></td>
							<td>State<font color="red">*</font></td>
							<td><input type="text" name="state"
								value="<%=user.getState()%>" required></td>
							<td>Country<font color="red">*</font></td>
							<td><input type="text" name="country"
								value="<%=user.getCountry()%>" required></td>
						</tr>
						<tr>
							<td>Pincode<font color="red">*</font></td>
							<td><input type="text" name="pincode" id="pincode"
								value="<%=user.getPinCode()%>" required></td>
						</tr>
						<tr>
							<td>Phone Number<font color="red">*</font></td>
							<td><input type="text" name="phonenumber"
								value="<%=user.getPhoneNumber()%>" required></td>
							<td>Email Id<font color="red">*</font></td>
							<td><input type="text" name="emailid" id="emailid"
								value="<%=user.getEmailId()%>" required></td>
						</tr>
					</table>
				</fieldset>
				<fieldset>
					<input type="submit" value="Submit"> &nbsp; &nbsp;

				</fieldset>
			</form>
			<%=request.getAttribute("updatestatus")==null?"":request.getAttribute("updatestatus") %>
		</div>

	</div>

</body>
</html>