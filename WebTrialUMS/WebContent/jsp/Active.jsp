<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Activation</title>
<style>
#submission {
	text-align: center;
}

#login {
	margin: 50px auto;
	width: 500px;
}

body {
	font: 18px tahoma;
	margin: 0;
	background: #FF9900;
}

fieldset {
	border: 3px solid #CC292A;
	background: White;
}

input[type="submit"] {
	font-family: tahoma;
	font-size: 18px;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 20%;
	height: 30px;
	color: black;
	align: center;
}
</style>
</head>
<body bgcolor="#B0E0E6">
	<h1 align="center">Activation page</h1>
	<form action="${pageContext.request.contextPath }/ActivateServlet"
		method="post">
		<div id="login">
			<fieldset>
				<table style="width: 80%">
					<tr>
						<td>Enter password:</td>
						<td><input type="password" name="password" id="password"
							required></td>
					</tr>
				</table>
				<br>
				<input type="submit" value="Submit" style="text-align: center;">
			</fieldset>
		</div>
	</form>
	<center><%=request.getAttribute("activestatus") == null ? "" : request.getAttribute("activestatus")%></center>
	<center>
		<a href="${pageContext.request.contextPath }/jsp/Login.jsp"
			style="color: black">Back to login page</a>
	</center>
</body>
</html>