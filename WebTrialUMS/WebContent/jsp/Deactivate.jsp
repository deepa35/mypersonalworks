<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
function validate()
{
	alert("You are about to deactivate your account");	
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Deactivation page</title>

<style>
body
{
font:18px tahoma;
margin:0;

}
ul.horizontal
{
	list-style-type: none;
    margin: 0;
    padding: 0;
    top:0;
    width: 100%;
    background-color: #FF9900;
    position:fixed;
   
    overflow: hidden;
    border: 3px solid #CC292A;

}
li.horizontal
{
	float: right;
}
li.horizontal a
{
	display:block;
	color:black;
	text-align:center;
	padding:14px 16px;
	text-decoration:none;
	border: 1px solid #CC292A;
}
li.horizontal a:hover:not(.active)
{
	background-color: #555;
    color: white;
}
.active
{
	background-color: #CC292A;
    color: white;
}
ul.vertical {
    list-style-type: none;
    margin: 0;
    padding: 0;
    width: 25%;
    background-color: #FF9900;
    position:fixed;
    height: 100%;
    overflow: auto;
    border: 3px solid #CC292A;
}

li.vertical a {
    display: block;
    color: #000;
    padding: 8px 0 8px 16px;
    text-decoration: none;
    border: 1px solid #CC292A;
}

li.vertical a.active {
    background-color: #CC292A;
    color: white;
}

li.vertical a:hover:not(.active) {
    background-color: #555;
    color: white;
    
}

input[type="submit"]

{
  font-family:tahoma;
  font-size:18px;
  border: 1px solid Black;
  border-radius: 55px;
  box-shadow: 0 1px #fff;
  background-color:#CC292A;
  width:15%;
  height:30px;
  color:black;
}
</style>
<% 
response.setHeader("Pragma","no-cache"); 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Expires","0"); 
response.setDateHeader("Expires",-1); 
%> 
</head>
<body>
<%try {
    if ( session.getAttribute("userid") == null ) {
    	 request.setAttribute("Error", "Your Session has ended. Please login again...");
    	 request.getRequestDispatcher("/jsp/Login.jsp").forward(request, response);
    return;    
    }
  } catch (Exception e) {
    e.printStackTrace();
    return;
  }%>
<ul class="horizontal">
	<li class="horizontal" style="float:left;display: block;text-align:center;padding:14px 16px;font-size:30px;">Welcome User</li>
	 <li class="horizontal"><a style="float:right;display: block;text-align:center;" href="${pageContext.request.contextPath }/LogOutServlet">Log Out</a></li>
</ul>

<ul class="vertical">
  <li class="vertical"><a href="${pageContext.request.contextPath }/jsp/UserHome.jsp">My Profile</a></li>
  <li class="vertical"><a href="${pageContext.request.contextPath }/jsp/EditProfileUser.jsp">Edit Profile</a></li>
  <li class="vertical"><a  class="active" href="${pageContext.request.contextPath }/jsp/Deactivate.jsp">Deactivate </a></li>
  <li class="vertical"><a href="${pageContext.request.contextPath }/jsp/ChangePasswordUser.jsp">Change Password</a></li> 
</ul>

<div style="margin-left:25%;margin-top:70px;padding:1px 16px;height:1000px; ">

<form action="${pageContext.request.contextPath }/DeactivateServlet" method="post" onsubmit="return validate()" ><br>
<table><tr><td>
Enter password:</td><td><input type="password" name="password" id="password" required></td></tr></table>
<br>
<input type="submit" value="Submit">
</form>
<%=request.getAttribute("deactstatus")==null?"":request.getAttribute("deactstatus") %>
</div>
</body>
</html>