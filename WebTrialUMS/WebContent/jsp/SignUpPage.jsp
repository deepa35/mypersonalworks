<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New User Registration</title>
<style>
#submission {
	text-align: center;
}

#login {
	margin: 50px auto;
	width: 75%;
}

body {
	font: 18px tahoma;
	margin: 0;
	background: #FF9900;
}

fieldset {
	border: 3px solid #CC292A;
	background: White;
}

input[type="submit"] {
	font-family: tahoma;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 15%;
	color: white;
}

input[type="reset"] {
	font-family: tahoma;
	border: 1px solid Black;
	border-radius: 55px;
	box-shadow: 0 1px #fff;
	background-color: #CC292A;
	width: 15%;
	color: white;
}
</style>
<script>
	function SignUpValidate() {

		alert("Are you sure you want to submit?");
		var name = document.getElementById("name").value;
		if (!/^[A-Za-z\s]+$/.test(name)) {
			alert("Name cannot contain number or special character");
			return false;
		}

		var gender = document.getElementsByName("gender");
		if (gender[0].checked == false && gender[1].checked == false) {
			alert("Please select gender");
			return false;
		}

		var name = document.getElementById("state").value;
		if (!/^[A-Za-z\s]+$/.test(name)) {
			alert("State cannot contain number or special character");
			return false;
		}

		var name = document.getElementById("country").value;
		if (!/^[A-Za-z\s]+$/.test(name)) {
			alert("Country cannot contain number or special character");
			return false;
		}

		var pincode = document.getElementById("pincode").value;
		if (isNaN(pincode) || (pincode.length !=5 && pincode.length!=6) ) {
			alert("Please enter a valid numeric pincode");
			return false;
		}

		var tel = document.signup.phonenumber.value;
		var phoneValid = tel.replace(/[^0-9]/g, '');
		if (isNaN(tel) == true || phoneValid.length != 10) {
			alert("Please enter valid phone number");
			return false;
		}

		var email = document.getElementById("emailid");
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email.value)) {
			alert("Please provide a valid email address");
			email.focus;
			return false;
		}

		var password = document.getElementById("password").value;

		var passlen = password.length;
		if (passlen < 6) {
			alert("Password is too short. Atleast 6 characters needed.");
			return false;
		}

		else if (password.search(/\d/) == -1) {
			alert("Atleast one digit needed in password");
			return false;
		} else if (password.search(/[a-zA-Z]/) == -1) {
			alert("Atleast one alphabet needed in password");
			return false;
		} else if (password.search(/[!\@\#\$\%\^\&\*\(\)\_\+]/) == -1) {
			alert("Atleast one special character needed in password");
			return false;
		}
		var confirmpassword = document.getElementById("confirmpassword").value;
		if (password != confirmpassword) {
			alert("Please make sure that your password and corfirmed password are the same");
			return false;
		}

		var declar = document.getElementsByName("declare");
		if (declar[0].checked == false) {
			alert("Please tick the checkbox to declare ");
			return false;
		}

	}
</script>
</head>
<body>
	<div id="login">
		<h2 align="center" style="text-decoration: underline">Sign Up
			Form</h2>
		<br>
		<p align="right">
			<font color="red">*</font>Mandatory Fields
		</p>
		<form action="${pageContext.request.contextPath }/UserSignupServlet"
			method="post" name="signup" onsubmit="return SignUpValidate()">
			<fieldset>
				<p style="text-decoration: underline">Personal Details</p>
				<table>
					<tr>

						<td>Name<font color="red">*</font></td>
						<td><input type="text" name="name" id="name" required></td>
					</tr>

					<tr>
						<td>Gender<font color="red">*</font></td>
						<td><input type="radio" name="gender" value="female">Female</td>
						<td><input type="radio" name="gender" value="male">Male</td>
					</tr>
					<tr>
						<td>Date Of Birth<font color="red">*</font></td>
						<td><input type="date" name="dateofbirth" id="dob" required></td>
					</tr>
					<tr>
						<td>Addressline 1<font color="red">*</font></td>
						<td><input type="text" name="address1" required></td>
					</tr>
					<tr>
						<td>City<font color="red">*</font></td>
						<td><input type="text" name="city" required></td>
						<td>State<font color="red">*</font></td>
						<td><input type="text" name="state" required id="state"></td>
						<td>Country<font color="red">*</font></td>
						<td><input type="text" name="country" required id="country"></td>
					</tr>
					<tr>
						<td>Pincode<font color="red">*</font></td>
						<td><input type="text" name="pincode" id="pincode" required></td>
					</tr>
					<tr>
						<td>Phone Number<font color="red">*</font></td>
						<td><input type="text" name="phonenumber" required></td>
						<td>Email Id<font color="red">*</font></td>
						<td><input type="text" name="emailid" id="emailid" required></td>
					</tr>
				</table>
			</fieldset>
			<fieldset>
				<p style="text-decoration: underline;">Login Credentials</p>
				<table>
					<tr>
						<td>Password<font color="red">*</font></td>
						<td><input type="password" name="password" id="password"
							required></td>
						<td style="font-size: 13px;">(Password should have atleast 6
							characters including a digit and a special character.)</td>
					</tr>
					<tr>
						<td>Confirm password<font color="red">*</font></td>
						<td><input type="password" name="confirmpassword"
							id="confirmpassword" required></td>
					</tr>
				</table>
			</fieldset>
			<fieldset>
				<br> <input type="checkbox" name="declare">Declaration:The
				above stated information is true to the best of my knowledge and
				belief.
			</fieldset>
			<fieldset>
				<br>
				<br> <input type="submit" value="Submit"
					style="font: 18px tahoma;"> &nbsp; &nbsp; <input
					type="reset" value="Reset" style="font: 18px tahoma;">
			</fieldset>
		</form>
	</div>
</body>
</html>