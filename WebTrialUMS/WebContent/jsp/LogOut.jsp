<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Logged Out</title>
<style>
#submission
{
text-align:center;
}
#login
{
margin: 50px auto;
width: 400px;

}
body
{
font:18px tahoma;
margin:0;
background:#FF9900;
}
fieldset
{
border:3px solid #CC292A;
background:White;
}
</style>
</head>
<body>
<h1 align="center">Logged Out</h1>
<div id="login">
<fieldset>
<p align="center"><b>You have successfully logged out</b></p>
<p align="center">
<a href="${pageContext.request.contextPath }/jsp/Login.jsp" style="color:black">Back to login</a></p>
</body>
</html>