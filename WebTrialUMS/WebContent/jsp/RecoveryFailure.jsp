<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Request Failed</title>
<style>
#submission {
	text-align: center;
}

#login {
	margin: 50px auto;
	width: 400px;
}

body {
	font: 18px tahoma;
	margin: 0;
	background: #FF9900;
}

fieldset {
	border: 3px solid #CC292A;
	background: White;
}
</style>
</head>
<body>
	<div id="login">
		<fieldset>
			<h1 align="center">Recovery failed</h1>
			<p align="center">
				<font color="red"><%=request.getAttribute("recoveryStatus") == null ? "" : request.getAttribute("recoveryStatus")%></font>
			</p>
		</fieldset>
	</div>
	<p align="center">
		<a href="${pageContext.request.contextPath }/jsp/PasswordRecovery.jsp"
			style="color: black">Back to Password Recovery</a>
	</p>
</body>
</html>