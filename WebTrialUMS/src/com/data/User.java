package com.data;

import java.time.LocalDate;
import java.time.Period;

/*
 * Holds data for each user object
 */
public class User {
	public static final int ACTIVE = 1;
	public static final int INACTIVE = 0;
	public static final String USER_ACT = "ACTIVE";
	public static final String USER_INACT = "INACTIVE";

	private int userId, age;
	private String password, name, gender, dob, address, city, state, country, pinCode, phoneNumber,
	emailId, activity, role;

	public User(int userId, String password, String name, String gender, String dob, String address,
			String city, String state, String country, String pinCode, String phoneNumber, String emailId,
			String activity, String role) {
		this.userId = userId;
		this.password = password;
		this.name = name;
		this.gender = gender;
		this.dob = dob;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pinCode = pinCode;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.activity = activity;
		this.role = role;
		this.age=ageCalculator(dob); 
	}

	private int ageCalculator(String dob) {
		int age = 0;
		if(dob!=null) {
			int year = Integer.parseInt(dob.substring(0, 4));
			int month = Integer.parseInt(dob.substring(5,7));
			int day = Integer.parseInt(dob.substring(8,10));

			LocalDate birthday = LocalDate.of(year, month, day);
			age = Period.between(birthday, LocalDate.now()).getYears();
		}
		return age;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public int getAge() {
		return age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getUserId() {
		return userId;
	}

}