package com.data;

/*
 * Object to be used only for login
 */
public class LoginPojo {
	private String userId;
	private String password;
	
	public LoginPojo(String userId, String password) {
		this.userId = userId;
		this.password = password;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public String getPassword() {
		return password;
	}
}
