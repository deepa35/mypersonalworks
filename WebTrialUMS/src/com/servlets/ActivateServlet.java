package com.servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connector.UserDAO;
import com.data.LoginPojo;
import com.util.FileList;
import com.util.Roles;

/**
 * Servlet implementation class Activate
 */
public class ActivateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActivateServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uId=request.getSession().getAttribute("userid").toString();
		int uniqueid = Integer.parseInt(uId);
		String password = request.getParameter("password");
		try {
			boolean userFound = UserDAO.searchUser(new LoginPojo(uId, password));
			if(userFound) {
				UserDAO.activate(uniqueid);
				if(UserDAO.getRole(uniqueid).equals(Roles.ADMIN.name())) {
					request.getRequestDispatcher(FileList.ADMIN_HOME.get()).forward(request, response);
				} else {
					request.getRequestDispatcher(FileList.USER_HOME.get()).forward(request, response);
				}
			} else {
				request.setAttribute("activestatus","The password you have entered is wrong. Try again");
				request.getRequestDispatcher(FileList.ACTIVE.get()).forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
