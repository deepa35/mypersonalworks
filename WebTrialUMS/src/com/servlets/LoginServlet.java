package com.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.connector.UserDAO;
import com.data.LoginPojo;
import com.data.User;
import com.util.FileList;
import com.util.Roles;


/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L; 
	private final String FAIL_MSG = "Invalid user credentials. Please check your username and password.";
	private final String INACTIVE_MSG = "User Inactive";

	public LoginServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String userName = request.getParameter("Username");
		String password = request.getParameter("Password");
		LoginPojo login = new LoginPojo(userName, password);
		try {
			boolean userFound = UserDAO.searchUser(login);

			if(userFound) {
				int userId = Integer.parseInt(userName);
				String role = UserDAO.getRole(userId);
				request.getSession().setAttribute("userid", userName);

				if(role.equals(Roles.ADMIN.name())) {
					request.getRequestDispatcher(FileList.USER_LIST.get()).forward(request, response);
				}else {
					int activity = UserDAO.getActivity(userId);

					if(activity == User.ACTIVE) {
						request.getRequestDispatcher(FileList.USER_HOME.get()).forward(request, response);
					} else {
						request.setAttribute("recoveryStatus", INACTIVE_MSG);
						request.getRequestDispatcher(FileList.DEACT_WARN.get()).forward(request, response);
					}
				}
			} else {
				request.setAttribute("invalidUser", FAIL_MSG); 
				request.getRequestDispatcher(FileList.LOGIN.get()).forward(request,response);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
