package com.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connector.UserDAO;
import com.util.FileList;

/**
 * Servlet implementation class PasswordRecovery
 */
public class PasswordRecServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PasswordRecServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		final String ERROR_MSG = "Invalid User/Admin. Please check the username and date of birth again.";
		String username = request.getParameter("usernamerec");
		String dob = request.getParameter("dob");
		
		try {
			String recoveredPassword = UserDAO.passwordRecovery(username,dob);

			if(recoveredPassword == null) {
				request.setAttribute("recoveryStatus",ERROR_MSG);
				request.getRequestDispatcher(FileList.REC_FAIL.get()).forward(request, response);
			}
			else {
				request.setAttribute("recoveryStatus", recoveredPassword);
				request.getRequestDispatcher(FileList.PWD_REC.get()).forward(request, response);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
