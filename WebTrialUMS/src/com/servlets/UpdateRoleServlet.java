package com.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.connector.UserDAO;
import com.util.FileList;

/**
 * Servlet implementation class AssignRoleServlet
 */
public class UpdateRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateRoleServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String unqid = request.getParameter("idselector");
		int uid=Integer.parseInt(unqid);
		String role=request.getParameter("roleselector");
		try {
			UserDAO.setRole(uid,role);
			request.setAttribute("rolestatus", "Successfully assigned the role");
			request.getRequestDispatcher(FileList.UPDATE_ROLE.get()).forward(request,response);
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
}
