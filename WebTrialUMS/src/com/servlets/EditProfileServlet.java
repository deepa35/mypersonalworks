package com.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.connector.UserDAO;
import com.data.User;
import com.util.FileList;
import com.util.Roles;

/**
 * Servlet implementation class EditProfileServlet
 */
public class EditProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditProfileServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dateofbirth");
		String address = request.getParameter("address1");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		String pincode = request.getParameter("pincode");
		String phonenumber = request.getParameter("phonenumber");
		String emailid = request.getParameter("emailid");
		String id = request.getSession().getAttribute("userid").toString();
		int userId = Integer.parseInt(id);

		User user = new User(userId,null,name,gender,dob, address, city, state, country, pincode, phonenumber,emailid, null, null);
		try {
			UserDAO.updateDetails(user);
			request.setAttribute("updatestatus","Details successfully updated");
			if(UserDAO.getRole(userId).equals(Roles.ADMIN.name())) {
				request.getRequestDispatcher(FileList.EDIT_ADMIN_PROF.get()).forward(request, response);
			} else {
				request.getRequestDispatcher(FileList.EDIT_USER_PROF.get()).forward(request, response);
			}	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}

