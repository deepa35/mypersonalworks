package com.servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.connector.UserDAO;
import com.data.LoginPojo;
import com.util.FileList;

/**
 * Servlet implementation class Deactivate
 */
public class DeactivateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeactivateServlet() {
        super();
    }
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String password = request.getParameter("password");
		String id = request.getSession().getAttribute("userid").toString();
		int uId = Integer.parseInt(id);
		
		try {
			if(UserDAO.searchUser(new LoginPojo(id,password)))	{
				UserDAO.deactivate(uId);
				request.setAttribute("deactstatus","Your account has been deactivated. Login to activate");
				HttpSession session=request.getSession();
				session.setAttribute("userid",null);
				session.invalidate();
				request.getRequestDispatcher(FileList.LOGIN.get()).forward(request, response);
			}
			else {
				request.setAttribute("deactstatus","Deactivation failed. Check your password");
				request.getRequestDispatcher(FileList.DEACT.get()).forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
