package com.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connector.UserDAO;
import com.util.Roles;


/**
 * Servlet implementation class ChangePasswordServlet
 */
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePasswordServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response)	{
		String newPassword=request.getParameter("newpassword");
		
		String id=request.getSession().getAttribute("userid").toString();
		int uId= Integer.parseInt(id);
		System.out.println(uId);
		try
		{
			boolean status = UserDAO.changePassword(newPassword,uId);
			if(status) {
				request.setAttribute("passwordchangestatus", "Password successfully changed");
			} else {
				request.setAttribute("passwordchangestatus", "Check the password you entered and retry");
			}
			
			if(UserDAO.getRole(uId).equals(Roles.ADMIN.name())){
				request.getRequestDispatcher("/jsp/ChangePassword.jsp").forward(request,response);
			} else	{
				request.getRequestDispatcher("/jsp/ChangePasswordUser.jsp").forward(request,response);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
