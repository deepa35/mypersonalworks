package com.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connector.UserDAO;
import com.data.User;
import com.util.FileList;
import com.util.Roles;

/**
 * Servlet implementation class UserSignup
 */
public class UserSignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSignupServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dateofbirth");
		String address1 = request.getParameter("address1");
		String address = address1;
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		String pincode = request.getParameter("pincode");
		String phonenumber = request.getParameter("phonenumber");
		String emailid = request.getParameter("emailid");
		String password = request.getParameter("password");
		try {
			int newId = idCreator();
			User user = new User(newId,password, name, gender, dob, address, city, state, country, pincode, phonenumber, emailid,Integer.toString(User.ACTIVE).toString() ,Roles.DEFAULT.name());
			UserDAO.createUser(user);
			request.setAttribute("idNumber", newId);
			request.getRequestDispatcher(FileList.USER_CREATION_SUCCESS.get()).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int idCreator() throws SQLException {
		int maxId = UserDAO.getMaxID();
		return maxId+1;
	}
}
