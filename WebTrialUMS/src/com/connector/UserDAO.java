package com.connector;
import com.data.LoginPojo;
import com.data.User;
import com.util.DbConnector;
import com.util.Roles;

import java.sql.*;
import java.util.ArrayList;

public class UserDAO {
	private static final String RETRIEVE = "select * from USERDETAILS where userid = ? and password = ?";
	private static final String GET_DATA = "select * from USERDETAILS where userid= ?";
	private static final String GET_ROLE = "select role from USERDETAILS where userid=?";
	private static final String PASSWORD_RECOVERY = "select password from USERDETAILS where userid=? and dob=?";
	private static final String GET_ACTIVITY = "select activity from USERDETAILS where userid = ?";
	private static final String UPDATE_DETAILS = "update USERDETAILS set name=?,gender=?,dob=?,age=?,address=?,city=?,state=?,country=?,pincode=?,phonenumber=?,emailid=? where userid=?";
	private static final String UPDATE_ROLE = "update USERDETAILS set role=? where userid=?";
	private static final String CHANGE_PWD = "update USERDETAILS set password=? where userid=?";
	private static final String RET_USERS_ACTIVE = "select userid,name,role from USERDETAILS where activity=1 and role!=?" ;
	private static final String RET_USERS = "select userid,name,phonenumber,emailid,role,activity from USERDETAILS where role!=?" ;
	private static final String ACTIVATE = "update USERDETAILS set activity=1 where userid = ?";
	private static final String DEACTIVATE = "update USERDETAILS set activity=0 where userid = ?";
	private static final String CREATE = "insert into USERDETAILS values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String MAX_ID="select max(userid) from USERDETAILS";


	private static final String IdUser="select userid from USERDETAILS where id=?";
	private static final String StoreCredentials = "insert into USERDETAILS values(?,?,?)";
	private static final String dobCheck="select dob from USERDETAILS where userid=?";
	private static final String CreateRole="insert into roledetails values(?)";
	private static final String DeleteRole="delete from roledetails where rolename=?";
	private static final String RoleCount="select count(*) from USERDETAILS group by role having role=?";
	private static final String getIDNotAssignedRole="select userid from USERDETAILS where role=? and activity=1 and userid>1";
	private static final String getIDWithRole="select userid from USERDETAILS where role!=? and activity=1 and userid>1";
	private static final String retrieveUserList = "select userid,name,phonenumber,emailid,role,activity from USERDETAILS where userid>1";
	private static final String retrieveUserListRoleNA = "select userid,name from USERDETAILS where userid>1 and activity=1 and role=?" ;
	private static final String setRole="update USERDETAILS set role=? where userid=?";
	private static final String Deactivate="select password from credentials where userid = ?";
	private static final String RoleCountRoleDetails="select count(*) from roledetails where rolename=?";

	public static boolean searchUser(LoginPojo user) throws SQLException{
		Connection con = null;
		PreparedStatement pStmt = null;
		boolean found = false;
		con = DbConnector.dbConnection();
		pStmt = con.prepareStatement(RETRIEVE);
		pStmt.setString(1, user.getUserId());
		pStmt.setString(2, user.getPassword());
		ResultSet rs = pStmt.executeQuery();

		if(rs.next()) {
			found = true;
		}

		pStmt.close();
		con.close();
		return found;
	}

	public static String passwordRecovery(String userId,String dob) throws SQLException {
		String pwdRecovered = null;
		Connection c = null;
		PreparedStatement pstmt = null;

		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(PASSWORD_RECOVERY);
		pstmt.setString(1,userId);
		pstmt.setString(2,dob);

		ResultSet rs=pstmt.executeQuery();
		if(rs.next()) {
			pwdRecovered = rs.getString(1);
		}
		pstmt.close();
		c.close();
		return pwdRecovered;
	}

	public static User getUserDetails(int id) throws SQLException {
		Connection c = null;
		PreparedStatement pstmt = null;
		c=DbConnector.dbConnection();
		pstmt = c.prepareStatement(GET_DATA);
		pstmt.setInt(1,id);
		ResultSet rs = pstmt.executeQuery();
		User user = null;
		if(rs.next()) {
			user = new User(rs.getInt(1), null,	rs.getString(2),rs.getString(3),
					rs.getString(4),rs.getString(6), rs.getString(7),rs.getString(8), 
					rs.getString(9),rs.getString(10), rs.getString(11), rs.getString(12),
					rs.getString(13), rs.getString(14));
		}

		pstmt.close();
		c.close();
		return user;
	}

	public static String getRole(int userName) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(GET_ROLE);
		pstmt.setInt(1, userName);
		String role = "";

		ResultSet rs=pstmt.executeQuery();
		if(rs.next()) {
			role = rs.getString(1);
		}
		pstmt.close();
		c.close();
		return role;
	}

	public static int getActivity(int userId) throws SQLException {
		Connection c = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int activity = User.INACTIVE;

		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(GET_ACTIVITY);
		pstmt.setInt(1,userId);
		rs=pstmt.executeQuery();

		if(rs.next()) {
			activity = rs.getInt(1);
		}

		pstmt.close();
		c.close();
		return activity;
	}

	public static void updateDetails(User u) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(UPDATE_DETAILS);
		pstmt.setString(1, u.getName());
		pstmt.setString(2,u.getGender());
		pstmt.setString(3, u.getDob());
		pstmt.setInt(4,u.getAge());
		pstmt.setString(5,u.getAddress());
		pstmt.setString(6,u.getCity());
		pstmt.setString(7,u.getState());
		pstmt.setString(8,u.getCountry());
		pstmt.setString(9,u.getPinCode());
		pstmt.setString(10,u.getPhoneNumber());
		pstmt.setString(11,u.getEmailId());
		pstmt.setInt(12,u.getUserId());
		pstmt.executeUpdate();
		pstmt.close();
		c.close();
	}

	public static void setRole(int unqid,String role) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(UPDATE_ROLE);
		pstmt.setString(1,role);
		pstmt.setInt(2,unqid);
		pstmt.executeUpdate();
		pstmt.close();
		c.close();
	}

	public static boolean changePassword(String newPassword,int uId) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		c=DbConnector.dbConnection();
		boolean status = false;
		pstmt=c.prepareStatement(CHANGE_PWD);
		pstmt.setString(1, newPassword);
		pstmt.setInt(2, uId);
		pstmt.executeUpdate();
		status = true;
		pstmt.close();
		c.close();
		return status;
	}  

	public static ArrayList<User> retrieveUserListActive() throws SQLException {
		Connection c=null;
		PreparedStatement pstmt =null;
		ArrayList<User> userlist = new ArrayList<>();
		c= DbConnector.dbConnection();
		pstmt=c.prepareStatement(RET_USERS_ACTIVE);
		pstmt.setString(1,Roles.ADMIN.name());
		ResultSet rs= pstmt.executeQuery();
		while(rs.next()) {
			User ur = new User(rs.getInt(1),null,rs.getString(2),null,null,null,null,null,null,null,null,null,null,rs.getString(3));
			userlist.add(ur);
		}
		pstmt.close();
		c.close();
		return userlist;
	}

	public static ArrayList<User> retrieveUserList() throws SQLException {
		Connection c=null;
		PreparedStatement pstmt =null;
		ArrayList<User> userlist = new ArrayList<>();
		c= DbConnector.dbConnection();
		pstmt=c.prepareStatement(RET_USERS);
		pstmt.setString(1,Roles.ADMIN.name());
		ResultSet rs= pstmt.executeQuery();
		while(rs.next()) {
			User ur = new User(rs.getInt(1),null,rs.getString(2),null,null,null,null,null,null,null,rs.getString(3),rs.getString(4),rs.getString(6),rs.getString(5));
			userlist.add(ur);
		}
		pstmt.close();
		c.close();
		return userlist;
	}

	public static void activate(int id) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;
		con = DbConnector.dbConnection();
		pStmt = con.prepareStatement(ACTIVATE);
		pStmt.setInt(1,id);
		pStmt.executeUpdate();
		pStmt.close();
		con.close();
	}

	public static void deactivate(int id) throws SQLException {
		Connection con = null;
		PreparedStatement pStmt = null;
		con = DbConnector.dbConnection();
		pStmt = con.prepareStatement(DEACTIVATE);
		pStmt.setInt(1,id);
		pStmt.executeUpdate();
		pStmt.close();
		con.close();
	}

	public static void createUser(User u) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(CREATE);
		pstmt.setInt(1,u.getUserId());
		pstmt.setString(2, u.getName());
		pstmt.setString(3,u.getGender());
		pstmt.setString(4, u.getDob());
		pstmt.setInt(5,u.getAge());
		pstmt.setString(6,u.getAddress());
		pstmt.setString(7,u.getCity());
		pstmt.setString(8,u.getState());
		pstmt.setString(9,u.getCountry());
		pstmt.setString(10,u.getPinCode());
		pstmt.setString(11,u.getPhoneNumber());
		pstmt.setString(12,u.getEmailId());
		pstmt.setString(13,u.getActivity());
		pstmt.setString(14,u.getRole());
		pstmt.setString(15,u.getPassword());
		pstmt.executeUpdate();
		pstmt.close();
		c.close();
	}

	public static int getMaxID() throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int maxId=0;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(MAX_ID);
		rs=pstmt.executeQuery();
		if(rs.next()) {
			maxId=rs.getInt(1);
		}
		pstmt.close();
		c.close();
		return maxId;
	}

	/*	public static int getIdLogin(LoginPojo u) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int maxId=0;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(CHECKLOGIN);
		pstmt.setInt(1, u.getUserId());
		pstmt.setString(2, u.getPassword());
		rs=pstmt.executeQuery();
		if(rs.next()) {
			maxId=rs.getInt(1);
		}
		pstmt.close();
		c.close();
		return maxId;
	}

	public static int roleCountRD(String rolename) throws SQLException {
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int rc=0;
		c=DbConnector.dbConnection();
		pstmt=c.prepareStatement(RoleCountRoleDetails);
		pstmt.setString(1, rolename);
		rs=pstmt.executeQuery();
		if(rs.next())
		{
			rc=rs.getInt(1);

		}
		pstmt.close();
		c.close();
		return rc;
	}

	public static ArrayList<UserDetails> retrieveUserList()throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt =null;

		ArrayList<UserDetails> userlist = new ArrayList<UserDetails>();
		try
		{
			c= DbConnector.dbConnection();
			pstmt=c.prepareStatement(retrieveUserList);
			ResultSet rs= pstmt.executeQuery();
			while(rs.next())
			{
				UserDetails ur=new UserDetails();
				ur.setUniqueid(rs.getInt(1));
				ur.setName(rs.getString(2));
				ur.setPhonenumber(rs.getLong(3));
				ur.setEmailid(rs.getString(4));
				ur.setRole(rs.getString(5));
				ur.setActivity(rs.getInt(6));

				userlist.add(ur);

			}

		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{

			pstmt.close();
			c.close();

		}
		return userlist;
	}

	public static ArrayList<UserDetails> retrieveUserListRoleNA()throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt =null;

		ArrayList<UserDetails> userlist = new ArrayList<UserDetails>();
		try
		{
			c= DbConnector.dbConnection();
			pstmt=c.prepareStatement(retrieveUserListRoleNA);
			pstmt.setString(1,"Not Assigned");
			ResultSet rs= pstmt.executeQuery();
			while(rs.next())
			{
				UserDetails ur=new UserDetails();
				ur.setUniqueid(rs.getInt(1));
				ur.setName(rs.getString(2));

				userlist.add(ur);

			}

		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{

			pstmt.close();
			c.close();

		}
		return userlist;
	}
	public static int getId(User u) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int maxId=0;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(IdUser);
			pstmt.setString(1, u.getUserName());
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				maxId=rs.getInt(1);

			}


		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return maxId;
	}


	public static void storeCredentials(User u)throws Exception
	{

		Connection c=null;
		PreparedStatement pstmt=null;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(StoreCredentials);
			int maxId=getmaxId();
			pstmt.setInt(1,++maxId);
			pstmt.setString(2,u.getUserName());
			pstmt.setString(3,u.getPassword());
			pstmt.executeUpdate();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
	} 

	public static String dobRetrieve(int id) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String dob=null;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(dobCheck);
			pstmt.setInt(1, id);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				dob=rs.getString(1);
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return dob;
	}
	public static void createRole(String role) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(CreateRole);
			pstmt.setString(1, role);
			pstmt.executeUpdate();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
	}
	public static void assignRole(String name,String role) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(AssignUpdateRole);
			pstmt.setString(1, role);
			pstmt.setString(2, name);
			pstmt.executeUpdate();
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
	}
	public static int getRoleCount(String role) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int roleCount=0;
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(RoleCount);
			pstmt.setString(1, role);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				roleCount=rs.getInt(1);
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return roleCount;
	}
	public static int deleteRole(String role) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		int roleCount=0;
		int status=0;
		try
		{
			c=DbConnector.dbConnection();
			roleCount=getRoleCount(role);
			pstmt=c.prepareStatement(DeleteRole);
			pstmt.setString(1, role);
			if(roleCount==0)
			{

				pstmt.executeUpdate();
				status=1;
			}
			else
			{

				status=0;
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}

		return status;
	}


	public static ArrayList<Integer> getIDNoRole() throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ArrayList<Integer> namelist = new ArrayList<Integer>();
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(getIDNotAssignedRole);
			pstmt.setString(1, "Not Assigned");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next())
			{
				namelist.add(rs.getInt(1));
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return namelist;
	}
	public static ArrayList<Integer> getIDWithRole() throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ArrayList<Integer> namelist = new ArrayList<Integer>();
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(getIDWithRole);
			pstmt.setString(1, "Not Assigned");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next())
			{
				namelist.add(rs.getInt(1));
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return namelist;
	}



	public static void updateuser(int id2) throws Exception {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DbConnector.dbConnection();
			pStmt = con.prepareStatement(UpdateActivate);
			pStmt.setInt(1,id2);

			pStmt.executeUpdate();
		} catch (Exception ex) {
			throw ex;
		} finally {
			pStmt.close();
			con.close();
		}
	}

	public static String deactivate(int id,String password) throws Exception
	{
		Connection c=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String pwd=null;
		String status="FAILURE";
		try
		{
			c=DbConnector.dbConnection();
			pstmt=c.prepareStatement(Deactivate);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next())
			{
				pwd=rs.getString(1);

			}
			if(pwd.equals(password))
			{
				status="SUCCESS";
			}

		}
		catch(Exception ex)
		{
			throw ex;
		}
		finally
		{
			pstmt.close();
			c.close();
		}
		return status;
	}

	 */
}