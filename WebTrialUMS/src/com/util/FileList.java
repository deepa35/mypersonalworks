package com.util;

public enum FileList {
	LOGIN("/jsp/Login.jsp"), ACTIVE("/jsp/Active.jsp"), ADMIN_HOME("/jsp/AdminHome.jsp"), 
	LOGOUT("/jsp/LogOut.jsp"), SIGNUP("/jsp/SignUpPage.jsp"), USER_HOME("/jsp/UserHome.jsp"),
	DEACT_WARN("/jsp/DeactivateWarning.jsp"), USER_LIST("/jsp/UserList.jsp"), REC_FAIL("/jsp/RecoveryFailure.jsp"),
	PWD_REC("/jsp/PasswordRecovery.jsp"), EDIT_ADMIN_PROF("/jsp/EditProfileAdmin.jsp"), EDIT_USER_PROF("/jsp/EditProfileUser.jsp"),
	UPDATE_ROLE("/jsp/UpdateRole.jsp"), USER_CREATION_SUCCESS("/jsp/UserCreationSuccess.jsp"), DEACT("/jsp/Deactivate.jsp");
	
	String filePath;
	
	FileList(String filePath) {
		this.filePath = filePath;
	}
	
	public String get() {
		return filePath;
	}
}
